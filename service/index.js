const express = require('express');
const app = express();

app.get('/', (req, res) => {
    res.send({
        ip: req.ip,
        headers: req.headers,
    });
});

app.listen(process.env.PORT, () => {
    console.log(`App on port ${process.env.PORT}`);
});
